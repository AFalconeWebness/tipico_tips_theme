const gulp = require("gulp");
const rename = require("gulp-rename");
const replace = require('gulp-replace');
const sass = require('gulp-sass')(require('sass'));
const postcss = require("gulp-postcss");
const cssnano = require("cssnano");
const { gulpSassError } = require('gulp-sass-error');
const uglify = require("gulp-uglify");
const terser = require("gulp-terser");
const sourcemaps = require('gulp-sourcemaps');
// const browsersync = require("browser-sync").create();
const themeKit = require("@shopify/themekit");

// Compile SCSS into CSS
function style() {
  // 1. Find SCSS files position
  return (
    gulp
      .src("src/styles/*.scss.liquid")
      // 2. Compile SCSS
      // a. Minify CSS file
      .pipe(
        sass(
          // Watch documentation for gulp-sass to set parameters as below
          /*
          {
          outputStyle: "compressed",
          }
          */
        )
          // b. Throw compile error on console
          .on("error", gulpSassError(true))
      )
      //.pipe(postcss([cssnano]))
      // c. Replace compiled file extension from .scss to .css

      .pipe(
        rename(function (path) {
          path.basename = path.basename.replace(".scss", ".css");
          path.extname = ".liquid";
        })
      )

      
      // d. Replaces interpolation characters needed to make liquid variables work within your Sass files without errors again to liquid compatible ones
      .pipe(replace('"{{', "{{"))
      .pipe(replace('}}"', "}}"))
      .pipe(replace('px"', "px"))
      // 3. Push file to correct folder
      .pipe(gulp.dest("src/assets"))
  );
}

// Compile JS to JS Minified
function js() {
  // 1. Find JS files position
  return (
    gulp
      .src("src/js/*.js", { sourcemaps: true })
      // 2. Compile JS
      // a. Minify JS file
      .pipe(uglify())
      .pipe(terser())
      // b. Replace compiled file extension from .js to .min.js
      .pipe(
        rename(function (path) {
          path.basename = path.basename.replace(".js", "");
          path.extname = ".min.js";
        })
      )
      // 3. Push file to correct folder
      .pipe(sourcemaps.write('../maps'))
      .pipe(gulp.dest("src/assets"))
  );
}

// Compile new HTML page live
// RUN THESE FUNCTIONS IF YOU DON'T WORK ON SHOPIFY
/*
function browsersyncServe(callback) {
  browsersync.init({
    server: {
      baseDir: '.'
    }
  });
}

function browsersyncReload(callback) {
  browsersync.reload();
  callback();
}
*/

function watch() {
  // RUN THESE FUNCTIONS IF YOU DON'T WORK ON SHOPIFY
  // browsersyncServe();
  // gulp.watch('*.html', browsersyncReload);
  gulp.watch("src/js", gulp.series(js));
  gulp.watch("src/styles", gulp.series(style));
  return themeKit.command("watch", {
    env: "development",
    allowLive: true,
    verbose: true
  });
}

gulp.task("watch", watch);

gulp.task("deploy", () => {
  return themeKit.command('deploy', {
    env: 'development'
  });
});

gulp.task("get", () => {
  return themeKit.command('get', {
    env: 'development'
  });
});
